package com.logistic.product;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;

import com.logistic.product.config.DozerConfig;
import com.logistic.product.config.SwaggerConfiguration;

//@SpringBootApplication
@SpringCloudApplication
public class Application {

	public static void main(String[] args) {
		new SpringApplicationBuilder(
				Application.class,
				//EurekaClientConfiguration.class,
				SwaggerConfiguration.class,
				DozerConfig.class
		).run(args);
	}
}
 