package com.logistic.product.helper;

/**
 * This class is responsible by manager all messages of the system that are in message.properties
 */
public class MessageHelper {
    private MessageHelper(){}
    public static final String ID_NOT_FOUND = "id.not.found";
    public static final String VALIDATION_FAILED = "validation.failed";
}
