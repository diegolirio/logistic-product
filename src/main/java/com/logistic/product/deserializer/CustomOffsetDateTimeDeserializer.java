package com.logistic.product.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.logistic.product.converter.StringToOffsetDateTimeConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.OffsetDateTime;


/**
 * Classe que deserealiza uma string de data e converte para um {@code Date}
 * @see <a href="http://www.baeldung.com/spring-type-conversions">Spring converters</a>
 * @see  <a href="https://stackoverflow.com/questions/35025550/register-spring-converter-programmatically-in-spring-boot">Registrando um conversor Spring<a/>
 * @return {@code Date}
 **/

@Component
public class CustomOffsetDateTimeDeserializer extends JsonDeserializer<OffsetDateTime> {

    @Autowired
    private StringToOffsetDateTimeConverter toOffsetDateTimeConverter;

    @Override
    public OffsetDateTime deserialize(final JsonParser p, final DeserializationContext ctxt)
            throws IOException {
        return toOffsetDateTimeConverter.convert(p.getValueAsString());
    }
}