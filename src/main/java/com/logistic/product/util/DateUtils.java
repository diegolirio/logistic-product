package com.logistic.product.util;

import com.logistic.product.converter.DateFormats;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DateUtils {

    public static Date toDate(String stringDate){
        List<String> formatStrings = DateFormats.getValues();
        for (String formatString : formatStrings){
            try{
                return new SimpleDateFormat(formatString).parse(stringDate);
            }catch (ParseException e){}
        }
        throw new RuntimeException("A data " +stringDate+ " está em um formato inválido ");
    }
}
