package com.logistic.product.orm;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection="products")
public class Product extends AbstractDocument {
	@NotEmpty
	private String code;
	@NotEmpty
	private String description;
	private BigDecimal valueUnit;
	private boolean active = true;
}
