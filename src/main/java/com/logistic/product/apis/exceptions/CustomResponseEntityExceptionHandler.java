package com.logistic.product.apis.exceptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.logistic.product.helper.MessageHelper;
import com.logistic.product.util.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestController
@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private Message message;

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllException(Exception ex, WebRequest request) {
		log.error("handleAllException()...");
		ExceptionResponse globalExceptionResponse = new ExceptionResponse<>(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(globalExceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ProductNotFoundException.class)
	public final ResponseEntity<Object> handleProductNotFoundException(ProductNotFoundException ex, WebRequest request) {
		log.error("handleProductNotFoundException()...");
		ExceptionResponse globalExceptionResponse = new ExceptionResponse<String>(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(globalExceptionResponse, HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
																  HttpHeaders headers, HttpStatus status, WebRequest request) {
		log.error("handleMethodArgumentNotValid()...");
		List<ErrorDetail> errors = this.toErrors(ex.getBindingResult());
		ExceptionResponse exceptionResponse = new ExceptionResponse<List<ErrorDetail>>(
				new Date(),
				this.message.getMessage(MessageHelper.VALIDATION_FAILED),
				errors
		);
		return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}

	private List<ErrorDetail> toErrors(BindingResult bindingResult){
		log.error("toErrors()...");
		List<ErrorDetail> errors = new ArrayList<>();
		Iterator<ObjectError> iterator = bindingResult.getAllErrors().iterator();
		while(iterator.hasNext()) {
			FieldError fieldError = (FieldError) iterator.next();
			ErrorDetail errorDetail = new ErrorDetail(
					fieldError.getField(),
					fieldError.getRejectedValue(),
					fieldError.getDefaultMessage()
			);
			errors.add(errorDetail);
			log.error(errorDetail.toString());
		}
		return errors;
	}

	@Data
	@ToString
	@AllArgsConstructor
	@NoArgsConstructor
	class ErrorDetail {
		private String field;
		private Object currentValue;
		private String message;
	}
	
}
