package com.logistic.product.apis.exceptions;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionResponse<T> {

	private Date timestamp;
	private String message;
	private T details;
	
}
