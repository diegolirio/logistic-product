package com.logistic.product.apis.exceptions;

public class ProductBadRequestException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ProductBadRequestException(String message) {
		super(message);
	}
	
}