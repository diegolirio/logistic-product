package com.logistic.product.apis.v1;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Relation(value="product", collectionRelation="products")
public class ProductResource extends ResourceSupport {
	private String code;
	private String description;
	private BigDecimal valueUnit;
	private boolean active;
	
}