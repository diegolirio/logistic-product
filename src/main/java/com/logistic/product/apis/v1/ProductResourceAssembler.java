package com.logistic.product.apis.v1;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import com.logistic.product.orm.Product;

@Component
public class ProductResourceAssembler extends ResourceAssemblerSupport<Product, ProductResource> {

    @Autowired
    private Mapper mapper;
	
	public ProductResourceAssembler() {
		super(ProductApiRestService.class, ProductResource.class);
	}
 
	@Override
	public ProductResource toResource(Product product) { 
		if(product == null) return null;
		ProductResource productResource = this.mapper.map(product, ProductResource.class);
		addLinks(productResource, product.getId());
		return productResource;
	}

	public List<ProductResource> toResource(List<Product> products) {
		List<ProductResource> listResource = new ArrayList<>();
		for (Product product : products) {
			listResource.add(toResource(product));
		}
		return listResource;
	}	
	
	public Product toDomain(ProductResource productResource) {
		return this.mapper.map(productResource, Product.class);
	}
	
    private void addLinks(ProductResource productResource, String id) {
    	productResource.add( linkTo(methodOn(ProductApiRestService.class).get(id)).withRel("Self") );
    	productResource.add( linkTo(methodOn(ProductApiRestService.class).get()).withRel("list") );
    }
}
