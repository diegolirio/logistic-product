package com.logistic.product.apis.v1;

import java.util.List;
import java.util.Optional;

import com.logistic.product.apis.exceptions.ProductNotFoundException;
import com.logistic.product.helper.MessageHelper;
import com.logistic.product.util.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.logistic.product.orm.Product;
import com.logistic.product.services.ProductService;

import io.swagger.annotations.Api;

import javax.validation.Valid;

@Slf4j
@CrossOrigin//(origins = "http://localhost:4200", maxAge = 4300)
@RestController
@Api(value="ProductApi")
@RequestMapping(ProductApiRestService.URL)
public class ProductApiRestService {
	
	public static final String URL = "/api/v1/products";
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductResourceAssembler productResourceAssembler;

	@Autowired
	private Message message;

	@GetMapping
	public ResponseEntity<List<ProductResource>> get() {
		log.info("get()..");
		return new ResponseEntity<>(productResourceAssembler.toResource(this.productService.findAll()), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ProductResource> get(@PathVariable("id") String id) {
		log.info("get({})...", id);
		Optional<Product> optionalProduct = this.productService.findOne(id);
		if(!optionalProduct.isPresent())
			throw new ProductNotFoundException(this.message.getMessage(MessageHelper.ID_NOT_FOUND, id));
		ProductResource resource = productResourceAssembler.toResource(optionalProduct.get());
		return new ResponseEntity<>(resource, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<ProductResource> post(@RequestBody ProductResource productResource) {
		log.info("post({})", productResource);
		Product product = productResourceAssembler.toDomain(productResource);
		product = this.productService.save(product);
		return new ResponseEntity<>(productResourceAssembler.toResource(product), HttpStatus.OK);
	}

	@PutMapping("{id}")
	public ResponseEntity<ProductResource> put(@PathVariable("id") String id, @Valid @RequestBody ProductResource productResource) {
		log.info("put({})...", productResource);
		Optional<Product> optionalProduct = this.productService.findOne(id);
		if(!optionalProduct.isPresent())
			throw new ProductNotFoundException(this.message.getMessage(MessageHelper.ID_NOT_FOUND, id));
		Product product = this.productService.update(optionalProduct.get(), productResource);
		return new ResponseEntity<>(productResourceAssembler.toResource(product), HttpStatus.OK);
	}

	@GetMapping("/code/{code}")
	public ResponseEntity<ProductResource> getByCode(@PathVariable("code") String code) {
		log.info("getByCode({})", code);
		Product product = this.productService.findByCode(code);
		ProductResource resource = productResourceAssembler.toResource(product);
		return new ResponseEntity<>(resource, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") String id) {
		log.info("delete({})", id);
		Optional<Product> optionalProduct = this.productService.findOne(id);
		if(!optionalProduct.isPresent())
			throw new ProductNotFoundException(this.message.getMessage(MessageHelper.ID_NOT_FOUND, id));
		this.productService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}	

	
}
