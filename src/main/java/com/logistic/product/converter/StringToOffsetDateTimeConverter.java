package com.logistic.product.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.Date;

import static com.logistic.product.util.DateUtils.toDate;

@Component
public class StringToOffsetDateTimeConverter implements Converter<String, OffsetDateTime> {

    @Autowired
    private DateToOffsetDateTimeConverter toOffsetDateTimeConverter;

    @Override
    public OffsetDateTime convert(String source) {
        OffsetDateTime offSetDateTime = null;
        Date data = toDate(source);
        offSetDateTime  = toOffsetDateTimeConverter.convert(data);
        return offSetDateTime;
    }
}