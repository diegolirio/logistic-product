package com.logistic.product.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {
 
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.logistic.product.apis.v1"))
				//.paths(PathSelectors.regex("/apis/v1.*")).build()
				.paths(PathSelectors.any()).build()
				.apiInfo(metaData());

	}

	private ApiInfo metaData() {
		return new ApiInfo("Product",
				"Product - REST API",
				"1.0",
				"Terms of service",
				new Contact("Logistic4Go", "https://bitbucket.org/logistic4go/logistic-product", "phoenix4go@gmail.com"),
				"Apache License Version 2.0",
				"https://www.apache.org/licenses/LICENSE-2.0");
	}
		
}