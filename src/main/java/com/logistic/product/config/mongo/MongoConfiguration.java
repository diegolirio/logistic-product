//package com.logistic.product.config.mongo;
//
//import com.mongodb.MongoClientOptions;
//import com.mongodb.MongoClientURI;
//import lombok.Data;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;
//import org.springframework.data.mongodb.MongoDbFactory;
//import org.springframework.data.mongodb.config.EnableMongoAuditing;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
//import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
//import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
//import com.mongodb.MongoClientOptions.Builder;
//
//import java.net.UnknownHostException;
//
//@Profile("!integration")
//@Data
//@Configuration
//@EnableMongoRepositories(basePackages = {"com.logistic.product.services"})
//@EnableMongoAuditing
//public class MongoConfiguration implements AbstractMongoConfig {
//
//    @Value("${mongo.default.uri}")
//    private String defaultMongoUri;
//
//    @Value("${mongo.default.socketTimeout}")
//    private int socketTimeout;
//
//    @Value("${mongo.default.socketTimeout}")
//    private int connectTimeout;
//
//    @Value("${mongo.default.socketTimeout}")
//    private int minHeartbeatFrequency;
//
//    @Value("${mongo.default.socketTimeout}")
//    private int heartbeatSocketTimeout;
//
//    public MongoDbFactory createMongoDbFactory()  {
//        Builder builder = MongoClientOptions.builder();
//        builder.socketTimeout(getSocketTimeout());
//        builder.connectTimeout(getConnectTimeout());
//        builder.minHeartbeatFrequency(getMinHeartbeatFrequency());
//        builder.heartbeatSocketTimeout(getHeartbeatSocketTimeout());
//        MongoClientURI mongoClientURI = new MongoClientURI(getDefaultMongoUri(), builder);
//        return new SimpleMongoDbFactory(mongoClientURI);
//    }
//
//    @Override
//    public MongoTemplate getMongoTemplate() throws UnknownHostException{
//        MongoTemplate mongoTemplate = new MongoTemplate(createMongoDbFactory());
//        MappingMongoConverter mmc = (MappingMongoConverter) mongoTemplate.getConverter();
//        mmc.setCustomConversions(customConversions());
//        mmc.afterPropertiesSet();
//        return mongoTemplate;
//    }
//}