package com.logistic.product.config.mongo;

import com.logistic.product.converter.DateToOffsetDateTimeConverter;
import com.logistic.product.converter.OffsetDateTimeToDateConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface AbstractMongoConfig {
    @Bean
    default MongoCustomConversions customConversions() {
        List<Converter<?,?>> converters = new ArrayList<>();
        converters.add(new OffsetDateTimeToDateConverter());
        converters.add(new DateToOffsetDateTimeConverter());
        return new MongoCustomConversions(converters);
    }

    MongoTemplate getMongoTemplate() throws IOException;
}
