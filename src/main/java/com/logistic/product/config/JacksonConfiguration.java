package com.logistic.product.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.logistic.product.deserializer.CustomOffsetDateTimeDeserializer;
import com.logistic.product.deserializer.CustomOffsetDateTimeSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.TimeZone;


/**
 * Classe para Configurar o Jackson para adaptação do novo sistema de datas
 */
@Configuration
public class JacksonConfiguration implements WebMvcConfigurer {

    @Autowired
    private CustomOffsetDateTimeDeserializer dateDeserializer;

    @Autowired
    private CustomOffsetDateTimeSerializer dateSerializer;


    public MappingJackson2HttpMessageConverter jacksonMessageConverter() {
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper()
                .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
                .configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
        configureDate(mapper);
        messageConverter.setObjectMapper(mapper);
        return messageConverter;
    }

    /**
     * Método que registra conversores de data para o Jacksson.
     * @see  <a href="https://stackoverflow.com/questions/46263773/jackson-parse-custom-offset-date-time">Registrando um conversor Spring<a/>
     * @param {@code ObjectMapper}
     * * */
    private void configureDate(ObjectMapper mapper) {
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addDeserializer(OffsetDateTime.class, this.dateDeserializer);
        javaTimeModule.addSerializer(OffsetDateTime.class, this.dateSerializer);
        mapper.registerModule(javaTimeModule).disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
                .setTimeZone(TimeZone.getDefault());;
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new ParameterNamesModule());
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(jacksonMessageConverter());
        WebMvcConfigurer.super.configureMessageConverters(converters);
    }

    @Bean(name="mapper")
    public ObjectMapper getObjectMapper() {
        return jacksonMessageConverter().getObjectMapper();
    }

}