package com.logistic.product.services;

import java.util.List;
import java.util.Optional;

import com.logistic.product.apis.v1.ProductResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logistic.product.orm.Product;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<Product> findAll() {
		return this.productRepository.findAll();
	}
 
	@Override
	public Product save(Product product) {
		return this.productRepository.save(product);
	}

	@Override
	public Optional<Product> findOne(String id) {
		return this.productRepository.findById(id);
	}

	@Override
	public void delete(String id) {
		this.productRepository.deleteById(id);
	}

	@Override
	public Product findByCode(String code) {
		return this.productRepository.findByCode(code);
	}

	@Override
	public Product update(Product product, ProductResource productResource) {
		product.setCode(productResource.getCode());
		product.setDescription(productResource.getDescription());
		product.setValueUnit(productResource.getValueUnit());
		return this.productRepository.save(product);
	}


}
