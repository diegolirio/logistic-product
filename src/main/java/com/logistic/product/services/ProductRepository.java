package com.logistic.product.services;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.logistic.product.orm.Product;

import java.util.Optional;

@Repository
public interface ProductRepository extends MongoRepository<Product, String> {
	Product findByCode(String code);
    Optional<Product> findByDescription(String description);
}

