package com.logistic.product.services;

import java.util.List;
import java.util.Optional;

import com.logistic.product.apis.v1.ProductResource;
import com.logistic.product.orm.Product;

public interface ProductService {
	List<Product> findAll();
	Product save(Product product);
	Optional<Product> findOne(String id);
	void delete(String id);
	Product findByCode(String code);
    Product update(Product product, ProductResource productResource);
}
