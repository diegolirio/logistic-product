package com.logistic.product.converter;

import com.logistic.product.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestContextManager;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

/**
 * Classe que parametriza os testes dos conversores de data.
 * */

@SpringBootTest(classes = {StringToOffsetDateTimeConverter.class,DateToOffsetDateTimeConverter.class})
@RunWith(Parameterized.class)
public class DateConverterTest {

    @Autowired
    private StringToOffsetDateTimeConverter dateConverter;

    @Parameterized.Parameter
    public String stringDate;

    @Parameterized.Parameter(value=1)
    public String cenario;

    private static String YYYYMMDD_T_HHMMSSX_LOCALZONE = "2018-05-01T10:30:00-03:00";
    private static String YYYYMMDD_T_HHMMSS_Z_UTC = "2018-05-01T10:30:00Z";
    private static String YYYYMMDD = "2018-05-01";
    private static String DDMMYYYY_HHMM = "01/05/2018 10:30";
    private static String DDMMYYYY = "01/05/2018";
    private TestContextManager testContextManager;

    @Before
    public void setupTests() throws Exception {
        this.testContextManager = new TestContextManager(getClass());
        this.testContextManager.prepareTestInstance(this);
    }

    @Parameterized.Parameters(name="{1}")
    public static Collection<Object[]> getParametros(){
        return Arrays.asList(new Object[][] {
                {DDMMYYYY,  "cenario = deve_converter_a_string_DDMMYYYY_para_date"},
                {YYYYMMDD_T_HHMMSSX_LOCALZONE,  "cenario = deve_converter_a_string_YYYYMMDD_T_HHMMSS_para_dateTime"},
                {YYYYMMDD_T_HHMMSS_Z_UTC,  "cenario = deve_converter_a_string_YYYYMMDD_T_HHMMSSZ_para_dateTime"},
                {YYYYMMDD,  "cenario = deve_converter_a_string_YYYYMMDD_para_date"},
                {DDMMYYYY_HHMM,  "cenario = deve_converter_a_string_DDMMYYYY_HHMM_para_dateTime"}
        });
    }

    @Test
    public void deve_testar_se_o_dia_mes_ano_e_o_timezone_estao_corretos_apos_a_conversao_de_uma_string_para_um_date() {
        OffsetDateTime date = dateConverter.convert(stringDate);
        assertTrue(date.getDayOfMonth() == 01);
        assertTrue(date.getMonthValue() == 05);
        assertTrue(date.getYear() == 2018);
        System.out.println(date.getOffset().toString());
        assertTrue(date.getOffset().toString().equals("-03:00"));
    }

    @Test
    public void deve_testar_se_a_hora_minuto_e_segundo_estao_corretos_apos_a_conversao_de_uma_string_para_um_date() {
        OffsetDateTime date = dateConverter.convert(stringDate);
        assumeTrue(date.getHour() > 0); // Executo o teste apenas se a data possuir hora, minuto e segundo...
        assertTrue(date.getHour() == 10);
        assertTrue(date.getMinute() == 30);
        assertTrue(date.getSecond() == 00);
    }
}