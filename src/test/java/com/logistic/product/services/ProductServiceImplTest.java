package com.logistic.product.services;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.logistic.product.fixture.templates.BaseTemplate;
import com.logistic.product.fixture.templates.ProductFixtureTemplate;
import com.logistic.product.orm.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

	@InjectMocks
	private ProductServiceImpl productService;

	@Mock
	private ProductRepository productRepository;

	private List<Product> list;

	private Product product;

	@Before
	public void before() {
		FixtureFactoryLoader.loadTemplates(BaseTemplate.TEMPLATE_BASE_PACKAGES);
		list =  Fixture.from(Product.class).gimme(150, ProductFixtureTemplate.LABEL_PRODUCT_VALID_WITH_ID);
		product =  Fixture.from(Product.class).gimme(ProductFixtureTemplate.LABEL_PRODUCT_VALID_WITHOUT_ID);
	}

	@Test
	public void testFindAll() {
		when(productRepository.findAll()).thenReturn(list);
		List<Product> findAll = productService.findAll();
		assertNotNull(findAll);
		verify(productRepository, times(1)).findAll();
	}

	@Test
	public void testSave() {
		when(productRepository.save(product)).thenReturn(product);
		Product saved = productService.save(product);
		assertNotNull(saved);
		verify(productRepository, times(1)).save(saved);
	}

	@Test
	public void testFindOne() {
		final String ID_VALUE = ProductFixtureTemplate.RANDOM_ID_VALUE;
		when(productRepository.findById(ID_VALUE)).thenReturn(Optional.of(product));
		Optional<Product> optionalProductResponse = productService.findOne(ID_VALUE);
		assertTrue(optionalProductResponse.isPresent());
		verify(productRepository, times(1)).findById(ID_VALUE);
	}

	@Test
	public void testDelete() {
		final String ID_VALUE = ProductFixtureTemplate.RANDOM_ID_VALUE;
		doNothing().when(productRepository).deleteById(ID_VALUE);
		productService.delete(ID_VALUE);
		verify(productRepository, times(1)).deleteById(ID_VALUE);
	}
}
