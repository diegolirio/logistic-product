package com.logistic.product.fixture.templates;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.logistic.product.orm.Product;

import java.math.BigDecimal;
import java.util.UUID;

public class ProductFixtureTemplate implements TemplateLoader {

    public static final String  LABEL_PRODUCT_VALID_WITH_ID = "LABEL_PRODUCT_VALID_WITH_ID";
    public static final String  LABEL_PRODUCT_VALID_WITHOUT_ID = "LABEL_PRODUCT_VALID_WITHOUT_ID";
    public static final String  DESCRIPTION = "NOTEBOOK";
    public static final String  RANDOM_ID_VALUE = UUID.randomUUID().toString();
    public static final String  RANDOM_CODE_VALUE = UUID.randomUUID().toString();
    public static final boolean ACTIVE_VALUE = true;

    public class ProductFields {
        public static final String ID = "id";
        public static final String CODE = "code";
        public static final String DESCRIPTION = "description";
        public static final String VALUE_UNIT = "valueUnit";
        public static final String ACTIVE = "active";
    }

    @Override
    public void load() {
        Fixture.of(Product.class).addTemplate(LABEL_PRODUCT_VALID_WITH_ID, new Rule() {
            {
                add(ProductFields.ID, RANDOM_ID_VALUE);
                add(ProductFields.CODE, RANDOM_CODE_VALUE);
                add(ProductFields.DESCRIPTION, DESCRIPTION);
                add(ProductFields.VALUE_UNIT, new BigDecimal(4000));
                add(ProductFields.ACTIVE, ACTIVE_VALUE);
            }
        });

        Fixture.of(Product.class).addTemplate(LABEL_PRODUCT_VALID_WITHOUT_ID, new Rule() {
            {
                add(ProductFields.CODE, RANDOM_CODE_VALUE);
                add(ProductFields.DESCRIPTION, DESCRIPTION);
                add(ProductFields.VALUE_UNIT, new BigDecimal(2000));
                add(ProductFields.ACTIVE, ACTIVE_VALUE);
            }
        });
    }
}
