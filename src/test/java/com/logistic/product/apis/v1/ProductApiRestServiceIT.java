package com.logistic.product.apis.v1;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.logistic.product.config.ProductIntegrationTest;
import com.logistic.product.fixture.templates.BaseTemplate;
import com.logistic.product.fixture.templates.ProductFixtureTemplate;
import com.logistic.product.helper.MessageHelper;
import com.logistic.product.orm.Product;
import com.logistic.product.services.ProductRepository;
import com.logistic.product.util.Message;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.UUID;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@ProductIntegrationTest
public class ProductApiRestServiceIT {

    static final String ERROR_TIMESTAMP = "timestamp";
    static final String ERROR_MESSAGE = "message";
    static final String URL_WITH_ID = ProductApiRestService.URL.concat("/{id}");
    static final String ID_NOT_FOUND =  UUID.randomUUID().toString();
    public static final String ERROR_DETAILS = "details";

    @LocalServerPort
    private int serverPort;

    @Autowired
    private ProductRepository repository;

    @Autowired
    private ProductResourceAssembler productResourceAssembler;

    @Autowired
    private Message message;

    private Product product;

    @BeforeClass
    public static void init(){
        FixtureFactoryLoader.loadTemplates(BaseTemplate.TEMPLATE_BASE_PACKAGES);
    }

    @Before
    public void setUp() {
        RestAssured.port = this.serverPort;
        this.repository.deleteAll();
        this.product = Fixture.from(Product.class).gimme(ProductFixtureTemplate.LABEL_PRODUCT_VALID_WITHOUT_ID);
        assertThat(this.product.getId(), isEmptyOrNullString());
        this.repository.save(this.product);
        assertThat(this.product.getId(), not(isEmptyOrNullString()));
    }

    @After
    public void destroy(){
        repository.deleteAll();
    }


    @Test
    public void test_get_all_should_return_list_of_products_with_all_required_properties_valid() {
        when().get(ProductApiRestService.URL)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType(ContentType.JSON)
                .assertThat()
                // products with all properties in response
                .body("size", is(greaterThan(0)))
                .body("[0]", hasKey(ProductFixtureTemplate.ProductFields.CODE))
                .body("[0]", hasKey(ProductFixtureTemplate.ProductFields.DESCRIPTION))
                .body("[0]", hasKey(ProductFixtureTemplate.ProductFields.VALUE_UNIT))
                .body("[0]", hasKey(ProductFixtureTemplate.ProductFields.ACTIVE))
                .and()
                // products and all required properties with values
                .body("[0].code", not(isEmptyOrNullString()))
                .body("[0].description", not(isEmptyOrNullString()))
                .body("[0].valueUnit", not(isEmptyOrNullString()))
                .body("[0].active", notNullValue());
    }


    @Test
    public void test_get_one_with_id_not_found_should_return_status_code_404() {
        given().pathParam(ProductFixtureTemplate.ProductFields.ID, ID_NOT_FOUND)
                .when().get(URL_WITH_ID)
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .assertThat()
                .body(notNullValue())
                .and()
                .body("$", hasKey(ERROR_TIMESTAMP))
                .body("$", hasKey(ERROR_MESSAGE))
                .body("$", hasKey(ERROR_DETAILS))
                .and()
                .body(ERROR_MESSAGE, containsString(message.getMessage(MessageHelper.ID_NOT_FOUND, ID_NOT_FOUND)))
                .body(ERROR_DETAILS, containsString("uri=".concat(ProductApiRestService.URL).concat("/").concat(ID_NOT_FOUND)));
    }

    @Test
    public void test_post_should_request_one_product_without_id_and_after_request_should_to_exist_one_product_on_repository(){
        this.repository.deleteAll();
        this.product = Fixture.from(Product.class).gimme(ProductFixtureTemplate.LABEL_PRODUCT_VALID_WITHOUT_ID);
        assertThat(this.product.getId(), isEmptyOrNullString());
        ProductResource productResource = this.productResourceAssembler.toResource(this.product);
        given()
                .body(productResource)
                .contentType(ContentType.JSON)
                .when()
                .post(ProductApiRestService.URL)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .assertThat()
                .body("code", comparesEqualTo(productResource.getCode()))
                .body("description", comparesEqualTo(productResource.getDescription()))
                .body("valueUnit", comparesEqualTo(productResource.getValueUnit().intValue()))
                .body("active", is(true));
        assertThat(this.repository.findAll().size(), is(greaterThan(0)));
    }


    @Test
    public void test_delete_should_return_status_code_200_and_not_should_exist_in_repository_after_delete(){
        assertThat(product.getId(), not(isEmptyOrNullString()));
        given().pathParam(ProductFixtureTemplate.ProductFields.ID, this.product.getId())
                .when()
                .delete(URL_WITH_ID)
                .then()
                .statusCode(HttpStatus.SC_OK);
        Optional<Product> productOptional = this.repository.findById(this.product.getId());
        assertThat(productOptional.isPresent(), comparesEqualTo(false));
    }

    @Test
    public void test_delete_with_id_not_found_should_return_status_code_404(){
        given().pathParam(ProductFixtureTemplate.ProductFields.ID, ID_NOT_FOUND)
                .when()
                .delete(URL_WITH_ID)
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .assertThat()
                .body(notNullValue())
                .and()
                .body("$", hasKey(ERROR_TIMESTAMP))
                .body("$", hasKey(ERROR_MESSAGE))
                .body("$", hasKey(ERROR_DETAILS))
                .and()
                .body(ERROR_MESSAGE, containsString(message.getMessage(MessageHelper.ID_NOT_FOUND, ID_NOT_FOUND)))
                .body(ERROR_DETAILS, containsString("uri=".concat(ProductApiRestService.URL).concat("/").concat(ID_NOT_FOUND)));
    }

    @Test
    public void test_put_updating_description_should_return_status_code_200_and_after_put_should_exist_in_repository_by_descriptio() {
        Optional<Product> optionalProduct = this.repository.findById(this.product.getId());
        assertThat(optionalProduct.isPresent(), is(true));
        String description = "Description-Updated";

        if(optionalProduct.isPresent()) {
            Product productToUpdate = optionalProduct.get();
            productToUpdate.setDescription(description);
            ProductResource productResource = this.productResourceAssembler.toResource(productToUpdate);
            given().pathParam(ProductFixtureTemplate.ProductFields.ID, productToUpdate.getId())
                    .body(productResource)
                    .contentType(ContentType.JSON)
                    .when()
                    .put(URL_WITH_ID)
                    .then()
                    .statusCode(HttpStatus.SC_OK)
                    .body(notNullValue());
        }
        assertThat(this.repository.findByDescription(description).isPresent(), comparesEqualTo(true));
    }

    @Test
    public void test_put_with_id_not_found_should_return_status_code_404_and_after_put_not_should_exist_in_repository_by_description() {
        Optional<Product> optionalProduct = this.repository.findById(this.product.getId());
        assertThat(optionalProduct.isPresent(), is(true));
        Product productToUpdate = null;
        String newDescription = "Description-Updated";
        if(optionalProduct.isPresent()) {
            productToUpdate = optionalProduct.get();
            productToUpdate.setDescription(newDescription);
        }

        ProductResource productResource = this.productResourceAssembler.toResource(productToUpdate);
        given().pathParam(ProductFixtureTemplate.ProductFields.ID, ID_NOT_FOUND)
                .body(productResource)
                .contentType(ContentType.JSON)
                .when()
                .put(URL_WITH_ID)
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .and()
                .body("$", hasKey(ERROR_TIMESTAMP))
                .body("$", hasKey(ERROR_MESSAGE))
                .body("$", hasKey(ERROR_DETAILS))
                .and()
                .body(ERROR_MESSAGE, containsString(message.getMessage(MessageHelper.ID_NOT_FOUND, ID_NOT_FOUND)))
                .body(ERROR_DETAILS, containsString("uri=".concat(ProductApiRestService.URL).concat("/").concat(ID_NOT_FOUND)));

        assertThat(this.repository.findByDescription(newDescription).isPresent(), comparesEqualTo(false));
    }

}
