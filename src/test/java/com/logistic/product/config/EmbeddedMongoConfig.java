package com.logistic.product.config;

import com.logistic.product.config.mongo.AbstractMongoConfig;
import com.mongodb.MongoClient;
import cz.jirutka.spring.embedmongo.EmbeddedMongoFactoryBean;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.io.IOException;



/**
 * This class configures the mongo embedded for integration profile.
 * @see <a href="https://springframework.guru/spring-boot-with-embedded-mongodb">Spring Guru</a>
 * @author Felipe Santaniello
 *
 * */
@Data
@Profile("integration")
@Configuration
@EnableMongoRepositories(basePackages = {"com.logistic.product.services"})
@EnableMongoAuditing
public class EmbeddedMongoConfig implements AbstractMongoConfig {

    @Value("${mongo.default.host}")
    private String host;

    @Value("${mongo.default.database}")
    private String database;

    @Value("${mongo.default.port}")
    private int port;

    @Override
    public MongoTemplate getMongoTemplate() throws IOException {
        EmbeddedMongoFactoryBean mongo = new EmbeddedMongoFactoryBean();
        mongo.setBindIp(this.host);
        mongo.setPort(this.port);
        MongoClient mongoClient = mongo.getObject();
        MongoTemplate mongoTemplate = new MongoTemplate(mongoClient, this.database);
        MappingMongoConverter mmc = (MappingMongoConverter) mongoTemplate.getConverter();
        mmc.setCustomConversions(customConversions());
        mmc.afterPropertiesSet();
        return mongoTemplate;
    }
}