package com.logistic.product.config;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


/**
 * This class configures the propertie and yml files to integration tests.
 * OBS: The @Value is not read by the spring in the class test, therefore, we need use this class in the integration test class.
 * @see <a href="http://www.baeldung.com/properties-with-spring">Baeldung</a>
 * @author Felipe Santaniello
 *
 * */
@Configuration
public class TestConfiguration {

    /**
     * This method get properties in the properties file and set in the test class.
     * @author Felipe Santaniello
     * */
    @Bean
    public static PropertySourcesPlaceholderConfigurer toPropertiesFile() {
        PropertySourcesPlaceholderConfigurer p = new PropertySourcesPlaceholderConfigurer();
        Resource[] resources = new ClassPathResource[ ]
                { new ClassPathResource("messages.properties") };
        p.setLocations( resources );
        p.setIgnoreUnresolvablePlaceholders(true);
        return p;
    }

    /**
     * This method get properties in the yml file and set in the test class.
     * @author Felipe Santaniello
     * */
    @Bean
    public static PropertySourcesPlaceholderConfigurer toYmlFile() {
        PropertySourcesPlaceholderConfigurer p = new PropertySourcesPlaceholderConfigurer();
        Resource[] resources = new ClassPathResource[ ]
                { new ClassPathResource( "application-integration.yml")};
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        yaml.setResources(resources);
        p.setProperties(yaml.getObject());
        p.setIgnoreUnresolvablePlaceholders(true);
        return p;
    }
}