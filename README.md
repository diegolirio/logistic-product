# Product Microservice

![Logistic Product Service](logistic-product.png)

### Mongo Setup
---------------

```json
use product;
db.createUser(
    {
      user: "product",
      pwd: "product",
      roles: [
         { "role" : "readWrite", "db" : "product" }
      ]
    }
)
```
---

### Development

[Seguir os requisitos minimos para cada microservices conforme este documento de "How to Do"](https://bitbucket.org/logistic4go/logistic-solution/src/master/doc/how-todo/minimal-required-to-develop-microservice.md)


---
### Build/Push Docker Image

```
mvn clean install -P production fabric8:build fabric8:push 
```

[C.D:](http://jenkins.phoenix4go.com/blue/organizations/jenkins/logistic-product/activity/) http://jenkins.phoenix4go.com/blue/organizations/jenkins/logistic-product/activity/

---




